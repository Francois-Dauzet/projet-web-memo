let grid = [];
let cells = [];
let choice = [];

// Emplacement dans le fichier html
let gridHTML = document.querySelector('#grid');

// Nombre colonne et ligne
const params = {
    row : 4,
    col : 4
};

// nombre d'image a upload
const imgCount = params.row * params.col / 2;

const images = [];

// Boucle de decompte pour les images à upload
for (let i = 0; i < imgCount; i++) {

    // upload des images en leurs assignant un index 2 part 2
    let url = `https://picsum.photos/id/${i + 50}/100/100`;

    // Assosiation de l'image avec un id
    const image = {
        url : url,
        id : i,
        index : i
    }

    // Double envoie url plus id
    images.push(image);
    image.index += 1;
    images.push(image);
}

// console.log('BEFORE DRAW GRID',images);

// Création du tableau
for (let i = 0 ; i < params.row  ; i++){

    // Création de l'élément row pour la page html
    const row = [];

    // Création d'une div pour l'envoie dans la page html
    const rowHTML = document.createElement('div');
    for (let j = 0 ; j < params.col ; j++){

        // Appel de la fonction random
        const randomImageIndex = randomIntFromInterval(0, images.length - 1);

        // Sauvegarde du ramdom image avant le splice
        const randomImage = images[randomImageIndex];

        // Pousser dans le tableau (row.push)
        row.push(images[randomImage]);

        // Création de l'element image
        const img = document.createElement('img');
        img.src = randomImage.url;
        img.dataset.id = randomImage.id;
        img.dataset.index = randomImage.index;
        img.dataset.finded = false;
        img.dataset.choosen = false;

        // Ajoute l'élément image comme enfant de l
        rowHTML.appendChild(img);
        cells.push(img);

        // supprime les données de l'ancien tableau transmise au nouveau tableau
        images.splice(randomImageIndex,1);

    }
    grid.push(row);
    gridHTML.appendChild(rowHTML);
}

// foreach pour lire toutes les image avec leurs id
cells.forEach(cell => {
    cell.addEventListener('click', () => {
        console.log('click');
        // choice[0] === choice[1]
        choice.push({
            id : cell.dataset.id,
            index : cell.dataset.index,
        })
        // console.log(choice);

        const cell1Choose = document.querySelector(`[data-index="${choice[0].index}"]`);
        cell1Choose.dataset.choosen = true;

        if (choice.length >= 2){


            // Si 
            if (choice[0].id === choice[1].id){
                console.log('win');
                //Récupère tous les id de toutes les images associé au choix
                const cellsWin = document.querySelectorAll(`[data-id="${choice[0].id}"]`);
                console.log(cellsWin);

                // Déclare true à l'image si deux id trouvé
                cellsWin.forEach(c => c.dataset.finded = true);
            }

            choice = [];
        }
    });
});

console.log(grid);

// Fonction de random pour les images
function randomIntFromInterval(min, max) { 
    return Math.floor(Math.random() * (max - min + 1) + min)
  }